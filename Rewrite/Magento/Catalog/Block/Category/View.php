<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CustomCategoryTitle\Rewrite\Magento\Catalog\Block\Category;

use  \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;

class View extends \Magento\Catalog\Block\Category\View
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Framework\App\RequestInterface $request
     * @param ScopeConfigInterface $scope
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Framework\App\RequestInterface $request,
        ScopeConfigInterface $scope,
        array $data = [],
    )
    {
        $this->_categoryHelper = $categoryHelper;
        $this->_catalogLayer = $layerResolver->get();
        $this->_coreRegistry = $registry;
        $this->request = $request;
        $this->_scope = $scope;
        parent::__construct($context, $layerResolver, $registry, $categoryHelper, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->getLayout()->createBlock(\Magento\Catalog\Block\Breadcrumbs::class);

        $category = $this->getCurrentCategory();
        if ($category) {

            if ($title = $category->getMetaTitle()) {
            } else {
                if ($this->isEnable()) {
                    if ($title = $category->getCategoryTitle()) {

                    } else {
                        $title = $category->getMetaTitle();
                    }
                } else {
                    $title = $category->getMetaTitle();
                }
            }

            if ($title) {
                $this->pageConfig->getTitle()->set($title);
            }

            $description = $category->getMetaDescription();
            if ($description) {
                $this->pageConfig->setDescription($description);
            }
            $keywords = $category->getMetaKeywords();
            if ($keywords) {
                $this->pageConfig->setKeywords($keywords);
            }
            if ($this->_categoryHelper->canUseCanonicalTag()) {
                $this->pageConfig->addRemotePageAsset(
                    $category->getUrl(),
                    'canonical',
                    ['attributes' => ['rel' => 'canonical']]
                );
            }

            $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
            if ($pageMainTitle) {
                if ($this->isEnable()) {
                    if ($name = $category->getCategoryTitle()){
                        $pageMainTitle->setPageTitle($name);
                    } else {
                        $pageMainTitle->setPageTitle($this->getCurrentCategory()->getName());
                    }
                } else {
                    $pageMainTitle->setPageTitle($this->getCurrentCategory()->getName());
                }
            }
        }

        return $this;
    }


    public function isEnable()
    {
        return $this->_scope->getValue('custom_category_title/settings/enable', ScopeInterface::SCOPE_STORE);
    }

    public function isHideDescription()
    {
        return $this->_scope->getValue('custom_category_title/settings/hide_description', ScopeInterface::SCOPE_STORE);
    }

    public function getCurentPage(){
        return $this->request->getParam("p");
    }
}

